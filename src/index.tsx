import React from 'react';
import ReactDOM from 'react-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { createWebStoragePersistor } from 'react-query/createWebStoragePersistor-experimental';
import { ReactQueryDevtools } from 'react-query/devtools';
import { persistQueryClient } from 'react-query/persistQueryClient-experimental';
import { GlobalStyles } from 'twin.macro';

import './fonts/roboto-light.css';
import './fonts/ciutadella-rounded-md/style.css';

import { App } from './components/App';
import reportWebVitals from './reportWebVitals';

// Forever return the cached image, even after page refreshes. Never refresh the data in the meantime.
// Sidenote: 'Never' in this case apparently means about 52 years, as React Query
//            does give a defined timestamp for this Infinity value.
export const CACHE_AND_STALE_TIME = Infinity;

// Create a query client
const queryClient = new QueryClient({
	defaultOptions: {
		queries: {
			cacheTime: CACHE_AND_STALE_TIME,
			// keepPreviousData: true,
			staleTime: CACHE_AND_STALE_TIME,
			// For now, let all errors be thrown for the error boundary to catch
			useErrorBoundary: true,
		},
	},
});

const localStoragePersistor = createWebStoragePersistor({ storage: window.localStorage });

persistQueryClient({
	queryClient,
	persistor: localStoragePersistor,
});

ReactDOM.render(
	<React.StrictMode>
		<GlobalStyles />
		<QueryClientProvider client={queryClient}>
			<App />
			<ReactQueryDevtools initialIsOpen={false} />
		</QueryClientProvider>
	</React.StrictMode>,
	document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
