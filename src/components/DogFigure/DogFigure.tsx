import React, { ReactElement, useEffect } from 'react';

import { useRandomDogImageUrlByBreed } from '../../hooks/useRandomDogImageUrlByBreed';
import { PetFigure } from '../PetFigure';
import { PetFigureData } from '../PetSearchForm';

export type DogFigureProps = {
	breedSearchTerm: string;
	onSuccess: () => void;
};

/**
 * Get data for rendering a random image of a certain dog breed.
 * @param breedSearchTerm
 * @constructor
 */
export function DogFigure({ breedSearchTerm, onSuccess }: DogFigureProps): ReactElement {
	const defaultPetFigureProps: PetFigureData = { breed: '', description: '', url: '' };
	const {
		data: { description: alt, breed, url: src } = defaultPetFigureProps,
		isLoading = true,
		isPreviousData = true,
	} = useRandomDogImageUrlByBreed(breedSearchTerm);

	useEffect(() => {
		if (src.length > 0) {
			onSuccess();
		}
	}, [onSuccess, src]);

	return <PetFigure alt={alt} breed={breed} isLoading={isLoading || isPreviousData} pet="dog" src={src} />;
}
