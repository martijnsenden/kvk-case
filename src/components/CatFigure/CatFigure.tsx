import React, { ReactElement, useEffect } from 'react';

import { useRandomCatImageUrlByBreed } from '../../hooks';
import { PetFigure } from '../PetFigure';
import { PetFigureData } from '../PetSearchForm';

export type CatFigureProps = {
	breedSearchTerm: string;
	onSuccess: () => void;
};

/**
 * Get data for rendering a random image of a certain cat breed .
 * @param breedSearchTerm
 * @constructor
 */
export function CatFigure({ breedSearchTerm, onSuccess }: CatFigureProps): ReactElement {
	const defaultPetFigureProps: PetFigureData = { breed: '', description: '', url: '' };
	const {
		data: { description: alt, breed, url: src } = defaultPetFigureProps,
		isLoading = true,
		isPreviousData = true,
	} = useRandomCatImageUrlByBreed(breedSearchTerm);

	useEffect(() => {
		if (src.length > 0) {
			onSuccess();
		}
	}, [onSuccess, src]);

	return <PetFigure alt={alt} breed={breed} isLoading={isLoading || isPreviousData} pet="cat" src={src} />;
}
