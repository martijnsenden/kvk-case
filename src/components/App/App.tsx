/** @jsxImportSource @emotion/react **/
import React from 'react';
import 'tailwindcss/tailwind.css';
import tw from 'twin.macro';

import { bodyText, headingStyles } from '../../styles';
import { Pet } from '../../types/pet';
import { PetSearchForm, PetSearchFormProps } from '../PetSearchForm';

export function App() {
	const catBreedSuggestions: PetSearchFormProps['breedSuggestions'] = ['balinese', 'javanese', 'persian', 'siamese'];
	const dogBreedSuggestions: PetSearchFormProps['breedSuggestions'] = ['boxer', 'hound', 'mastiff', 'poodle'];

	const containerStyles = [
		bodyText,
		tw`
			gap-1.5
			grid
			grid-cols-1
			gridTemplateRows[auto 1fr]
			lg:(
				gridTemplateColumns[1fr minmax(500px, 2fr) 1fr minmax(500px, 2fr) 1fr]
			)
			min-h-screen
			p-2
			sm:(
				gridTemplateColumns[1fr minmax(500px, 2fr) 1fr]
				p-0
			)
			w-full
		`,
	];

	const headerStyles = [
		headingStyles,
		tw`
			col-start-1
			col-end-2
			lg:(
				col-end-5
			)
			sm:(
				col-start-2
				col-end-3
			)
		`,
	];

	const petSearchGridColumnLayoutVariants = {
		cat: tw`
			lg:(
				row-start-2
				row-end-3
			)
		`,
		dog: tw`
			lg:(
				col-end-5
				col-start-4
			)
		`,
	};

	const petSearchGridColumnLayout = (pet: Pet) => [
		tw`
			col-end-2
			col-start-1
			sm:(
				col-end-3
				col-start-2
			)
		`,
		petSearchGridColumnLayoutVariants[pet],
	];

	return (
		<main css={containerStyles}>
			<h1 css={headerStyles}>Opdracht KVK—door Martijn Senden</h1>
			<PetSearchForm
				breedSuggestions={dogBreedSuggestions}
				gridColumnLayout={petSearchGridColumnLayout('dog')}
				pet="dog"
			/>
			<PetSearchForm
				breedSuggestions={catBreedSuggestions}
				gridColumnLayout={petSearchGridColumnLayout('cat')}
				pet="cat"
			/>
		</main>
	);
}
