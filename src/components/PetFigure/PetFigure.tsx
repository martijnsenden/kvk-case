/** @jsxImportSource @emotion/react **/
import React from 'react';
import tw from 'twin.macro';

import { Pet } from '../../types/pet';
import loader from './images/loader.svg';

export type PetFigureProps = {
	alt: string;
	breed: string;
	isLoading: boolean;
	pet: Pet;
	src: string;
};

const figureStyles = tw`
	grid
	gridAutoRows[minmax(min-content, max-content)]
	gridTemplateColumns[1fr auto 1fr]
	my-12
`;

const figureChildrenStyles = tw`
	col-end-3
	col-start-2
`;

const paragraphStyles = tw`
	text-center
`;

export function PetFigure({ alt, breed, isLoading, pet, src }: PetFigureProps) {
	return (
		<figure css={figureStyles}>
			<img alt={alt} css={figureChildrenStyles} src={src} />
			<figcaption css={figureChildrenStyles}>
				{isLoading ? (
					<>
						<object type="image/svg+xml" data={loader}>
							loader animation
						</object>
						<p css={paragraphStyles}>loading</p>
					</>
				) : (
					`${breed}${alt ? `: ${alt}` : ''}`
				)}
			</figcaption>
		</figure>
	);
}
