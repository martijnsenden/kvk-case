/** @jsxImportSource @emotion/react **/
import React, { ReactElement } from 'react';
import tw from 'twin.macro';

import { buttonStyles } from '../../styles';

export type PetFigureErrorProps = {
	error: unknown;
	resetErrorBoundary: () => void;
};

const containerStyles = tw`
	col-end-2
	col-start-1
	grid
	gridAutoRows[minmax(min-content, max-content)]
	gridTemplateColumns[1fr auto 1fr]
	my-12
	sm:(
		col-end-3
		col-start-2
	)
`;

const messageStyles = tw`
	col-end-3
	col-start-2
	dark:(
		text-red-500
	)
	font-bold
	text-red-700
`;

const retryButtonStyles = [
	buttonStyles,
	tw`
	col-end-3
	col-start-2
	mt-4
`,
];

/**
 * Renders an error message to be shown when an error occurred while fetching a random image of a certain dog breed.
 * @param error
 * @constructor
 */
export function PetFigureError({ error, resetErrorBoundary }: PetFigureErrorProps): ReactElement {
	return (
		<div css={containerStyles}>
			<p css={messageStyles}>{'\u2757'} Oeps, dit ras kunnen we niet vinden.</p>
			<button css={retryButtonStyles} onClick={resetErrorBoundary}>
				Probeer opnieuw
			</button>
		</div>
	);
}
