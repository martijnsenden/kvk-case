/** @jsxImportSource @emotion/react **/
import React, { ReactElement, useEffect, useRef, useState } from 'react';
import { ErrorBoundary } from 'react-error-boundary';
import tw, { TwStyle } from 'twin.macro';

import { buttonStyles } from '../../styles';
import { FixedLengthArray } from '../../types/array';
import { Pet } from '../../types/pet';
import { CatFigure } from '../CatFigure';
import { DogFigure } from '../DogFigure';
import { PetFigureError } from '../PetFigureError';

export type PetSearchFormProps = {
	breedSuggestions: FixedLengthArray<[string, string, string, string]>;
	gridColumnLayout: TwStyle[];
	pet: Pet;
};

export type PetFigureData = {
	breed: string;
	description: string;
	url: string;
};

const breedTextInputStyles = tw`
		bg-gray-200
		col-end-2
		col-start-1
		dark:(
			bg-gray-700
			text-gray-200
			disabled:(
				bg-gray-900
				text-gray-700
			)
		)
		disabled:(
			bg-gray-300
			text-gray-400
			cursor-not-allowed
		)
		p-2
		sm:(
			col-end-3
		)
		text-gray-700
	`;

const labelStyles = tw`
	gap-3
	grid
	grid-cols-1
	sm:(
		grid-cols-3
	)
`;

const submitButtonStyles = [
	buttonStyles,
	tw`
		col-end-2
		col-start-1
		sm:(
			col-end-4
			col-start-3
		)
	`,
];

const paragraphStyles = tw`
	col-end-2
	col-start-1
	sm:(
		col-end-3
	)
`;

export function PetSearchForm({ breedSuggestions, gridColumnLayout, pet }: PetSearchFormProps): ReactElement {
	const [breedSearchTerm, setBreedSearchTerm] = useState('');
	const [submitting, setSubmitting] = useState(false);
	const [submittedBreedSearchTerm, setSubmittedBreedSearchTerm] = useState(breedSearchTerm);
	const [hasJustReset, setHasJustReset] = useState(false);
	const petInputElement = useRef<HTMLInputElement>(null);

	const PetFigure = {
		cat: CatFigure,
		dog: DogFigure,
	}[pet];

	useEffect(() => {
		petInputElement.current?.focus();
	}, []);

	useEffect(() => {
		if (hasJustReset === true) {
			petInputElement.current?.focus();
			setHasJustReset(false);
		}
	}, [hasJustReset, setHasJustReset]);

	return (
		<div css={gridColumnLayout}>
			<form
				css={gridColumnLayout}
				name={`${pet}-search-form`}
				onSubmit={(event) => {
					event.preventDefault();
					setSubmitting(true);
					setSubmittedBreedSearchTerm(breedSearchTerm);
				}}
			>
				<label css={labelStyles}>
					<input
						disabled={submitting}
						minLength={pet === 'cat' ? 3 : 1}
						onChange={(event) => {
							setBreedSearchTerm(event.target.value);
						}}
						placeholder={`${pet === 'cat' ? 'Katten' : 'Honden'}ras`}
						ref={petInputElement}
						required
						css={breedTextInputStyles}
						type="text"
						value={breedSearchTerm}
					/>
					<input
						css={submitButtonStyles}
						disabled={submitting}
						type="submit"
						value={`Zoek ${pet === 'cat' ? 'kat' : 'hond'}`}
					/>
					<p css={paragraphStyles}>
						Probeer bijvoorbeeld <em>{breedSuggestions[0]}</em>, <em>{breedSuggestions[1]}</em>,{' '}
						<em>{breedSuggestions[2]}</em> of <em>{breedSuggestions[3]}</em>
					</p>
				</label>
			</form>
			<ErrorBoundary
				FallbackComponent={PetFigureError}
				onReset={() => {
					setBreedSearchTerm('');
					setSubmittedBreedSearchTerm('');
					setSubmitting(false);
					setHasJustReset(true);
				}}
			>
				<PetFigure
					breedSearchTerm={submittedBreedSearchTerm}
					onSuccess={() => {
						setSubmitting(false);
					}}
				/>
			</ErrorBoundary>
		</div>
	);
}
