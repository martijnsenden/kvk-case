export function createMemoizedGetRandomItemFromArray<T = void>(): (array: T[], staleTime: number) => T {
	function getRandomItemFromArray<T>(array: T[]): T {
		return array[Math.floor(Math.random() * array.length)];
	}

	const cache = new Map<
		T[],
		{
			before: number;
			cachedRandomItem: T;
		}
	>();

	return function memoizedGetRandomItemFromArray(array: T[], staleTime: number = 0) {
		function getNewRandomItemAndUpdateCache() {
			const randomItem = getRandomItemFromArray(array);
			cache.set(array, {
				before: Date.now(),
				cachedRandomItem: randomItem,
			});
			return randomItem;
		}

		if (cache.has(array)) {
			const cachedArray = cache.get(array);
			if (Date.now() <= cachedArray!.before + staleTime) {
				// The array is in the cache and staleTime for it has not passed yet.
				// Take the item from the cache.
				return cachedArray!.cachedRandomItem;
			} else {
				// The array is in the cache, but the staleTime for it has passed.
				// Get a new randomItem from the array and update the cache.
				return getNewRandomItemAndUpdateCache();
			}
		} else {
			// The array is not yet in the cache.
			// Get a new random item from the given array, and store everything in the cache.
			return getNewRandomItemAndUpdateCache();
		}
	};
}
