// See https://stackoverflow.com/a/59906630/1954860 for reasoning. This FixedLengthArray prevents accessing indexes
// outside of the initial boundaries and removes all array methods that allow changing the amount of items in the array.
type ArrayLengthMutationKeys = 'splice' | 'push' | 'pop' | 'shift' | 'unshift' | number;
type ArrayItems<T extends Array<any>> = T extends Array<infer TItems> ? TItems : never;

export type FixedLengthArray<T extends any[]> = Pick<T, Exclude<keyof T, ArrayLengthMutationKeys>> & {
	[Symbol.iterator]: () => IterableIterator<ArrayItems<T>>;
};
