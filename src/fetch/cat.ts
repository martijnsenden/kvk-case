import axios, { AxiosResponse } from 'axios';

import { CatBreedApiResponse } from './types/CatBreed';
import { CatImageApiResponse } from './types/CatImage';

export function fetchRandomCatImageIdByBreedChunk(breedChunk: string): Promise<CatBreedApiResponse> {
	return axios
		.get(`https://api.thecatapi.com/v1/breeds/search?q=${breedChunk}`)
		.then(
			(response: AxiosResponse) =>
				// Get the first breed that satisfies the search query and also has a reference image id
				response.data.find((item: { reference_image_id?: string }) => item.reference_image_id !== undefined) ||
				{},
		)
		.catch(() => ({}));
}

export function fetchCatImageByImageId(imageId: string): Promise<CatImageApiResponse> {
	return axios
		.get(`https://api.thecatapi.com/v1/images/${imageId}`)
		.then((response: AxiosResponse) => response.data || {});
}
