export type CatBreedApiResponse = {
	description: string;
	name: string;
	reference_image_id: string;
};
