import { CatBreedApiResponse } from './CatBreed';

export type CatImageApiResponse = {
	breeds: CatBreedApiResponse[];
	height: number;
	id: string;
	url: string;
	width: number;
};
