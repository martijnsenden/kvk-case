import axios, { AxiosResponse } from 'axios';
import { DogBreedImageUrls } from './types/DogBreedImageUrls';

const apiRoot = 'https://dog.ceo/api/';

export function fetchDogImageUrlsByBreed(breed: string): Promise<DogBreedImageUrls> {
	return axios.get(`${apiRoot}breed/${breed}/images`).then((response: AxiosResponse) => response.data);
}
