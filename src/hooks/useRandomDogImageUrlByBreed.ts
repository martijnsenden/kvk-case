import { useRef } from 'react';
import { useQuery } from 'react-query';

import { PetFigureData } from '../components/PetSearchForm';
import { fetchDogImageUrlsByBreed } from '../fetch';
import { createMemoizedGetRandomItemFromArray } from '../helpers/array';
import { CACHE_AND_STALE_TIME } from '../index';

export function useRandomDogImageUrlByBreed(breedSearchTerm: string) {
	const breedSearchTermLowerCase = breedSearchTerm.toLowerCase();
	const { current: memoizedGetRandomItemFromArray } = useRef(createMemoizedGetRandomItemFromArray<string>());

	return useQuery(['dogImageUrlsByBreed', breedSearchTermLowerCase], () => {
		if (breedSearchTermLowerCase.length > 0) {
			return fetchDogImageUrlsByBreed(breedSearchTermLowerCase).then(
				(dogBreedImageUrlsApiResponse): PetFigureData => {
					// Memoize the random image url forever, just like I do with all the queries. This means that when a new query
					// to fetch all the dog image urls is done after the staleTime has passed, and even though this may possibly be
					// for the same breed (and thus return an equal array of image urls), a new random image will be taken from
					// the array in this case. Since the time is set to Infinity, (see definition of CACHE_AND_STALE_TIME), this
					// only occurs after about 52 years ;-). If you want to test in the browser whether indeed other images are
					// loaded if the staletime is different, you can tweak it of course. Or delete the react query cache data in
					// the local storage in the developer tools and refresh the page.

					const url = memoizedGetRandomItemFromArray(
						dogBreedImageUrlsApiResponse.message,
						CACHE_AND_STALE_TIME,
					);
					const subBreed = url.match(/\/breeds\/([a-z]+)-([a-z]+)\//)?.[2] || '';
					return {
						description: subBreed,
						breed: breedSearchTermLowerCase,
						url,
					};
				},
			);
		}

		return {
			description: '',
			breed: breedSearchTermLowerCase,
			url: '',
		};
	});
}
