import { useQuery } from 'react-query';

import { PetFigureData } from '../components/PetSearchForm';
import { fetchCatImageByImageId, fetchRandomCatImageIdByBreedChunk } from '../fetch';

export function useRandomCatImageUrlByBreed(breedChunk: string) {
	const { data: imageId = '' } = useQuery(['randomCatImageIdByBreed', breedChunk], () => {
		if (!breedChunk || breedChunk.length === 0) {
			return 'zero length';
		}

		return fetchRandomCatImageIdByBreedChunk(breedChunk).then((catApiResponse): string => {
			if (catApiResponse?.reference_image_id === undefined) {
				return 'not found';
			}

			return catApiResponse?.reference_image_id;
		});
	});

	return useQuery(
		['catImageByImageId', imageId],
		() =>
			fetchCatImageByImageId(imageId).then((catImageApiResponse): PetFigureData => {
				if (imageId === 'not found') {
					return {
						breed: breedChunk,
						description: '',
						url: '',
					};
				}
				const {
					breeds: [{ description, name: breed }],
					url,
				} = catImageApiResponse;

				return {
					breed,
					description,
					url,
				};
			}),
		{
			// Do not execute the query until the imageId exists
			enabled: !!imageId && imageId !== 'zero length',
		},
	);
}
