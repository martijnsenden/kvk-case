import tw from 'twin.macro';

export const headingStyles = tw`
	color[#00526e]
	dark:(
		color[#007EA8]
	)
	font-headings
	py-12
	text-5xl
`;
