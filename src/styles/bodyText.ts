import tw from 'twin.macro';

export const bodyText = tw`
	bg-white
	dark:(
		bg-gray-800
		text-white
	)
	text-black
`;
