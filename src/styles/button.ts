import tw from 'twin.macro';

export const buttonStyles = tw`
	background[#aa418c]
	cursor-pointer
	disabled:(
		cursor-not-allowed
		background[#d4b4cb]
		hover:(
			background[#d4b4cb]
			scale-100
		)
		text-gray-400
	)
	font-bold
	hover:(
		background[#6b2958]
		scale-105
	)
	p-2
	rounded
	text-white
	transform
	transition-transform
	transitionDuration[400ms]
`;
