const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
	purge: false, // This is managed by twin.macro
	theme: {
		extend: {
			fontFamily: {
				sans: ['Roboto', ...defaultTheme.fontFamily.sans],
				headings: ['Ciutadella Rounded', ...defaultTheme.fontFamily.sans],
			},
		},
	},
	variants: {},
	plugins: [],
};
