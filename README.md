# [Opdracht KVK—door Martijn Senden](https://kvk-case-martijn-senden.netlify.app)

## Gekozen tools en technieken

### [Create React App](https://github.com/facebook/create-react-app)

Om niet te veel tijd kwijt te zijn aan het opzetten van alle tooling.
Ik heb wel [craco](https://github.com/gsoft-inc/craco) gebruikt om her
en der wat dingen te kunnen tweaken.

### [TypeScript](https://www.typescriptlang.org/docs/)

[TypeScript](https://www.typescriptlang.org/docs/) zorgt voor het valideren van alle statische types in je applicatie.
Zo helpt [TypeScript](https://www.typescriptlang.org/docs/) je bijvoorbeeld om zeker te zijn dat je nergens functies
aanroept met de verkeerde argumenten. Door hoe [TypeScript](https://www.typescriptlang.org/docs/) werkt is het in
feite al een belangrijke eerste automatische testmethode (met uitstekende editor-integratie).

### Code style

Ik heb [Prettier](https://prettier.io/docs/en/) gebruikt om de code style consistent te maken. Het stopt voortdurende
debatten over code styles door alle code om te zetten in een standaardstijl die slechts marginaal geconfigureerd
kan worden.

### Styling

Ik heb gebruik gemaakt van [Tailwind CSS](https://v2.tailwindcss.com/docs) in
combinatie met [twin.macro](https://github.com/ben-rogerson/twin.macro).
[Twin.macro](https://github.com/ben-rogerson/twin.macro) maakt het mogelijk om
classes van tailwind simpel te combineren met CSS-in-JS, bijvoorbeeld met [Styled
Components](https://styled-components.com/docs) of, zoals ik heb gedaan,
met [Emotion](https://emotion.sh/docs/introduction). Zo kun je atomic css gebruiken,
maar zonder [de downsides](https://jxnblk.com/blog/two-steps-forward/).
[Twin.macro](https://github.com/ben-rogerson/twin.macro) draait als build-script en
gooit alle ongebruikte styles uit je bundle. Zo krijg je echt alleen de stijlen die
je nodig hebt en blijft de bundle klein.

### Asynchrone, globale state

Ik heb [React Query](https://react-query.tanstack.com/overview) gebruikt omdat het
[buitengewoon geschikt](https://tkdodo.eu/blog/react-query-as-a-state-manager) is
voor het managen van globale, asynchrone state. In dit geval heb ik het gebruikt voor
het cachen van de responses op requests aan de beide api's. Ik heb de caching ingesteld op `Infinity`,
dus als je andere plaatjes voor een ras wilt zien dan wat je al eerder hebt gezocht,
dan moet je in je `localstorage` de key `REACT_QUERY_OFFLINE_CACHE` verwijderen en de
pagina verversen. Daarna is kiest hij steeds weer een random hond van het ras dat je zoekt.

### Layout

Ik heb [CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/) gebruikt om
de layout enigszins netjes en responsive te maken. Dit kan natuurlijk altijd nog beter,
maar het werkt al best aardig zo.

### Data-fetching

Ik heb [Axios](https://axios-http.com/docs/intro) gebruikt om het data-fetchen simpel te houden.

### Repo, CI en Deployment

Het [GIT-repo voor deze site](https://gitlab.com/martijnsenden/kvk-case) staat op [GitLab](https://gitlab.com).
Daar is ook een CI-pipeline ingericht. Deze voert voor iedere commit bij een Merge Request
linting uit, valideert de code style, test de statische typing (TypeScript), runt (aanwezige) tests, audit packages op vulnerabilities en bouwt de applicatie. Als er fouten
optreden, kan het Merge Request niet gemerged worden.

Als er een Merge Request naar de `master`-branch wordt gemerged, dan draait de pipeline een automatische deploy
naar [Netlify](netlify.com), waar de app gehost is.

## Available Scripts (uit Create React App Readme)

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will
remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right
into your project so you have full control over them. All of the commands except `eject` will still work, but they will
point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you
shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t
customize it when you are ready for it.
